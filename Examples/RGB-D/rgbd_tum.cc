/**
* This file is part of ORB-SLAM2.
*
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY;s without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/


#include<iostream>
#include<algorithm>
#include<fstream>
#include<chrono>



#include <Eigen/Dense>
#include <cmath>
#include <opencv2/core/core.hpp>
#include <librealsense2/rs.hpp>
#include <librealsense2/h/rs_internal.h>
#include <opencv2/opencv.hpp> 
#include <opencv2/core/eigen.hpp>
#include <pcl/point_types.h>
#include <pcl/common/common_headers.h>
#include <pcl/common/transforms.h>
#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/filters/passthrough.h>
#include <pcl/visualization/pcl_visualizer.h>

#include <System.h>
#include <Converter.h>
#include <lcm/lcm-cpp.hpp>
#include "references_realsense_t.hpp"

#define HEIGHT_GRID 200
#define WIDTH_GRID 200
#define THRESHOLD_GRID 300.0
#define MAX_SIZE_OF_PATH 1000
#define CLOSE_POINT_THRESHOLD 12
#define ANGLE_THRESHOLD 0.4
#define ANGLE_THRESHOLD2 0.25
#define AVG_SIZE 5

#define FINISH_Z 45
#define FINISH_X 65

#define AVG_SIZE_VEL 5

using namespace std;

using pcl_ptr = pcl::PointCloud<pcl::PointXYZ>::Ptr;

Eigen::MatrixXf grid = Eigen::MatrixXf::Zero(200,200);
Eigen::MatrixXf grid_for_line = Eigen::MatrixXf::Zero(200,200);
Eigen::MatrixXi Free_grid = Eigen::MatrixXi::Zero(200,200);

Eigen::MatrixXi True_grid = Eigen::MatrixXi::Zero(200,200);
Eigen::MatrixXi Inflated_grid = Eigen::MatrixXi::Zero(200,200);
Eigen::Matrix4f R = Eigen::Matrix4f::Identity();    //rotation for floor

float camera_x = 0;
float camera_y = 0;
float camera_z = 0;
float camera_phi = 0; // angle to z-axis, right (to x) is positive
int camera_index_x = 100;   //index of camera in world grid...
int camera_index_z = 0;
bool Arrived_at_finish = false;
float velocity = 0;

pair<int,int> pathway[HEIGHT_GRID][WIDTH_GRID]; // grid for BFS search
pair<int,int> planned_path[MAX_SIZE_OF_PATH];
int len_of_planned_path = -1;   // -1 always if no path currently available

lcm::LCM lcm_tag("udpm://239.255.76.67:7667?ttl=255");

float pitch_avg[AVG_SIZE] = {0.0,0.0,0.0,0.0,0.0};
float roll_avg[AVG_SIZE] = {0.0,0.0,0.0,0.0,0.0};
float pitch_speed = 0;
float roll_speed = 0;

float vel_avg[AVG_SIZE_VEL] = {0,0,0,0,0};
float yaw_avg[AVG_SIZE_VEL] = {0,0,0,0,0};


bool delay_algorithm = 0;

void send_references(int64_t timestamp, float velocity, float yaw_rate);

void update_pitch_roll_speed(Eigen::Matrix3f rot){
    float new_yaw=atan2(rot(1,0),rot(0,0));
    float new_pitch=atan2(-rot(2,0),sqrt(pow(R(2,0),2)+pow(rot(2,2),2)));
    float new_roll=atan2(rot(2,1),rot(2,2));
    cout << "yaw " << new_yaw << ",pitch " << new_pitch << ", roll " << new_roll << endl;
    
    for(int i=1;i<AVG_SIZE;i++){
        pitch_avg[i-1] = pitch_avg[i];
        roll_avg[i-1] = roll_avg[i];
    }
    pitch_avg[AVG_SIZE-1] = new_pitch;
    roll_avg[AVG_SIZE-1] = new_roll;
    pitch_speed = pitch_avg[AVG_SIZE-1] - pitch_avg[0];
    roll_speed = roll_avg[AVG_SIZE-1] - roll_avg[0];
    cout << "pitch_speed is "<<pitch_speed<<", roll_speed is "<<roll_speed<<endl;
}

pcl_ptr points_to_pcl(const rs2::points& points)
{
    pcl_ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);

    auto sp = points.get_profile().as<rs2::video_stream_profile>();
    cloud->width = sp.width();
    cloud->height = sp.height();
    cloud->is_dense = false;
    cloud->points.resize(points.size());
    auto ptr = points.get_vertices();
    for (auto& p : cloud->points)
    {   
        p.x = ptr->x;
        p.y = ptr->y;
        p.z = ptr->z;
        ptr++;
    }

    return cloud;
}

// find point on path close to camera that is closer than threshold
pair<int,int> find_close_path_point(){
    for(int i=len_of_planned_path-1;i>=0;i--){
        if(pow(planned_path[i].first - camera_index_z,2) + pow(planned_path[i].second - camera_index_x,2) < CLOSE_POINT_THRESHOLD){
            if(i == len_of_planned_path-1) Arrived_at_finish = true;
            return planned_path[i];
        }
    }
    return make_pair(-1,-1);
}

// control loop for giving references to sk8o motors
void driving_sk8o(){
    float yaw_rate = 0;
    if(len_of_planned_path == -1){
        send_references(0,0,0);
        return;
    }
    pair<int,int> close_point = find_close_path_point();
    if(close_point == planned_path[len_of_planned_path - 1]){
        cout << "arrived at the end";
        send_references(0,0,0);
        return;
    }

    int diff_z = close_point.first - camera_index_z;
    int diff_x = close_point.second - camera_index_x;
    float diff_angle =  atan2(diff_x,diff_z) - camera_phi;
    cout << "diff angle is " << diff_angle << endl;
    float dist = pow(diff_z,2)+pow(diff_x,2);
    if(abs(diff_angle) > ANGLE_THRESHOLD){
        if(velocity > 1) velocity -= 0.3;
        else velocity = 0;
        /*if(diff_angle > 0) yaw_rate = 1;
        else yaw_rate = -1;*/
    }else if(abs(diff_angle) > ANGLE_THRESHOLD2){
        if(velocity > 3) velocity -= 0.1;
        else if(velocity < 3) velocity += 0.1;
        
        /*if(diff_angle > 0) yaw_rate = 0.5;
        else yaw_rate = -0.5;*/
    }else{
        if(velocity < 3){
            velocity = 3;
        }else if(velocity >= 3 && velocity < 6){
            velocity += 0.05;
        }
    }
    yaw_rate = diff_angle*1.5;
    yaw_rate = min(max(yaw_rate,(float)-1.0),(float)1.0);
    send_references(0,velocity,yaw_rate);
}

void bresenham_line_Low(int j_null, int i_null, int j, int i){
    int dx = j - j_null; int ddd = dx;
    int dz = i - i_null;
    int zi = 1;
    if(dz < 0){
        zi = -1;
        dz = -dz;
    }
    int D = 2*dz - dx;
    int z = i_null;
    for(int x = j_null;x!=j;x = x + (ddd > 0) - (ddd < 0)){   // weird expresion is sign()
        if(True_grid(z,x) < 1){ // plotting a free cell
            Free_grid(z,x) = -1;
        }else{
            break;
        }
        if(D > 0){
            z = z+zi;
            D = D + 2*(dz-dx);
        }else{
            D = D + 2*dz;
        }
    }
}

void bresenham_line_High(int j_null, int i_null, int j, int i){
    int dx = j - j_null; 
    int dz = i - i_null;int ddd = dz;
    int xi = 1;
    if(dx < 0){
        xi = -1;
        dx = -dx;
    }
    int D = 2*dx - dz;
    int x = j_null;
    for(int z = i_null;z!=i;z = z + (ddd > 0) - (ddd < 0)){   // weird expresion is sign()
        if(True_grid(z,x) < 1){ // plotting a free cell
            Free_grid(z,x) = -1;
        }else{
            break;
        }
        if(D > 0){
            x = x+xi;
            D = D + 2*(dx-dz);
        }else{
            D = D + 2*dx;
        }
    }
}



void update_true_grid(){
    // init True grid on zero
    for(long int i=0;i<HEIGHT_GRID;i++){
        for(long int j=0;j<WIDTH_GRID;j++){
            True_grid(i,j) = 0;
        }
    }
    for(long int i=0;i<HEIGHT_GRID;i++){
        for(long int j=0;j<WIDTH_GRID;j++){
            if(grid(i,j)>THRESHOLD_GRID){
                True_grid(i,j) = 1;
            }
        }
    }
    True_grid(camera_index_z,camera_index_x) = 0;
    for(long int i=0;i<HEIGHT_GRID;i++){
        for(long int j=0;j<WIDTH_GRID;j++){
            if(grid_for_line(i,j) > THRESHOLD_GRID){
                int j_null = camera_index_x;
                int i_null = camera_index_z;
                
                if(abs(i - i_null) < abs( j - j_null)){
                    if(j_null > j){
                        bresenham_line_Low(j,i,j_null,i_null);
                    }else{
                        bresenham_line_Low(j_null,i_null,j,i);
                    }
                }else{
                    if(i_null > i){
                        bresenham_line_High(j,i,j_null,i_null);
                    }else{
                        bresenham_line_High(j_null,i_null,j,i);
                    }
                }
            }
        }
    }
    for(long int i=0;i<HEIGHT_GRID;i++){
        for(long int j=0;j<WIDTH_GRID;j++){
            if(Free_grid(i,j) == -1 && True_grid(i,j)!= 1){
                True_grid(i,j) = -1;
            }
        }
    }
}

/* inflates each obstacle by param-squares in each direction */
void inflate_grid(int inflate_by){
    for(long int i=0;i<HEIGHT_GRID;i++){
        for(long int j=0;j<WIDTH_GRID;j++){
            Inflated_grid(i,j) = 0;
        }
    }
    for(long int i=0;i<HEIGHT_GRID;i++){
        for(long int j=0;j<WIDTH_GRID;j++){
            if(True_grid(i,j)>0){
                for(int k = i-inflate_by;k<=i+inflate_by;k++){
                    for(int l = j-inflate_by;l<=j+inflate_by;l++){
                        if(k >= 0 && k < HEIGHT_GRID && l >= 0 && l < WIDTH_GRID){
                            Inflated_grid(k,l) = 1;
                        }
                    }
                }
            }
        }
    }
    for(long int i=0;i<HEIGHT_GRID;i++){
        for(long int j=0;j<WIDTH_GRID;j++){
            if(True_grid(i,j) == -1 && Inflated_grid(i,j) == 0){
                Inflated_grid(i,j) = -1;
            }
        }
    }
}

// right now it is BFS, maybe can become better; not needed, it takes 1 ms
// fills planned path with the found path, not including start
int search_algo(pair<int,int> start, pair<int,int> finish, bool Plan_only_free_cells){
    if(Inflated_grid(finish.first,finish.second) == 1){
        cout << "finish is on occupied square !!!"<<endl;
        len_of_planned_path = -1;
        send_references(0,0,0);
        return 0;
    }

    // ordered such that first are 4-axis than diagonals so that path is found preferably in 4-axis, unless longer
    int directions[8][2] = {{-1,0},{0,-1},{0,1},{1,0},{1,1},{-1,1},{1,-1},{-1,-1}};
    queue<pair<int,int>> q;
    q.push(start);

    for(int i=0;i<HEIGHT_GRID;i++){
        for(int j=0;j<WIDTH_GRID;j++){
            pathway[i][j] = make_pair(-1,-1);
        }
    }
    bool found = 0;
    while(q.size() > 0){
        pair<int, int> p = q.front();
        q.pop();
        
        if(p.first == finish.first){
            found = 1;
            finish.second = p.second;
            break;
        }   
        for(int i=0;i<8;i++){
            int a = p.first + directions[i][0];
            int b = p.second + directions[i][1];
            if (a >= 0 && b >= 0 && a < HEIGHT_GRID && b < WIDTH_GRID && pathway[a][b].first == -1 && Inflated_grid(a,b) < 1) {
                if(Plan_only_free_cells && Inflated_grid(a,b) == 0) continue;
                q.push(make_pair(a, b));
                pathway[a][b] = p;
            }
        }

    }
    if(found == 0){
        len_of_planned_path = -1;
        send_references(0,0,0);
        return 0;
    }

    pair<int,int> cur;
    cur.first = finish.first;   cur.second = finish.second;
    pair<int,int> provisional_path[MAX_SIZE_OF_PATH];

    int ind = 0;
    while(cur != start){
        provisional_path[ind].first = cur.first;
        provisional_path[ind].second = cur.second;
        int new_i = pathway[cur.first][cur.second].first;
        int new_j = pathway[cur.first][cur.second].second;
        cur.first = new_i;
        cur.second = new_j;
        if(cur.first == -1){
            break;
        }
        cout << new_i << " " << new_j << "track" <<endl;
        ind++;
    }
    for(int i=0;i<ind;i++){
        planned_path[i] = provisional_path[ind-1-i];
    }
    len_of_planned_path = ind;
    return 1;
}

// cross product of a,b put into p, used for floor alignment
void crossProduct(float vect_A[], float vect_B[], float cross_P[])
{
    cross_P[0] = vect_A[1] * vect_B[2] - vect_A[2] * vect_B[1];
    cross_P[1] = vect_A[2] * vect_B[0] - vect_A[0] * vect_B[2];
    cross_P[2] = vect_A[0] * vect_B[1] - vect_A[1] * vect_B[0];
}

float len_mine(float pointA[]){
    return sqrtf32(pow(pointA[0],2)+pow(pointA[1],2)+pow(pointA[2],2));
}

void make_rotation(float pointA[3],float pointB[3],float pointC[3]){
    pointC[0] = pointC[0] - pointA[0];
    pointC[1] = pointC[1] - pointA[1];
    pointC[2] = pointC[2] - pointA[2];

    pointB[0] = pointB[0] - pointA[0];
    pointB[1] = pointB[1] - pointA[1];
    pointB[2] = pointB[2] - pointA[2];

    crossProduct(pointB,pointC,pointA);

    float length = len_mine(pointA);
    pointA[0] = -pointA[0]/length;
    pointA[1] = -pointA[1]/length;
    pointA[2] = -pointA[2]/length;
    cout << pointA[0] << " " << pointA[1] << " " << pointA[2] << "end of cross product\n";

    float reference[3];
    reference[0] = 0;
    reference[1] = -1;
    reference[2] = 0;

    Eigen::Matrix3f id = Eigen::Matrix3f::Identity();
    Eigen::Matrix3f V_mat;
    float v[3];
    crossProduct(pointA,reference,v);
    V_mat(0,0) = 0;V_mat(1,1) = 0;V_mat(2,2) = 0;
    V_mat(0,1) = -v[2]; V_mat(1,0) = v[2];
    V_mat(0,2) = v[1]; V_mat(2,0) = -v[1];
    V_mat(1,2) = -v[0]; V_mat(2,1) = v[0];
    float num = (1 - (reference[0]*pointA[0]+reference[1]*pointA[1]+reference[2]*pointA[2]))/pow(len_mine(v),2);
    R.block(0,0,3,3) = id + V_mat + V_mat*V_mat*num;
    cout << endl << R << "this is the rotation" << endl;
}

void add_points_to_grid(pcl_ptr cloud){
    /*for(long int i=0;i<HEIGHT_GRID;i++){
        for(long int j=0;j<WIDTH_GRID;j++){
            grid(i,j) = (int) 0.98*grid(i,j);
        }
    }*/
    for(long int i=0;i<HEIGHT_GRID;i++){
        for(long int j=0;j<WIDTH_GRID;j++){
            grid_for_line(i,j) = 0;
        }
    }
    for (auto& p : cloud->points){
        int index_z = (int) (p.z * 10);
        int index_x = (int) (p.x * 10 + 100);
        if(p.y > 0 || p.y < -1){
            continue;
            cout << "thrown away y="<<p.y;
        }
        if(p.z < 0 || p.z > 9){
            continue;
            cout << "thrown away z="<<p.z;
        }
        if(p.x < -9 || p.x > 9){
            continue;
            cout << "thrown away x="<<p.x;
        }
        grid(index_z,index_x) += 1;
        grid_for_line(index_z,index_x) += 1;
    }
    // exlude grid point where camera is, often points default to there (0,0,0) if they cannot be determined by realsense algo
    grid(camera_index_z,camera_index_x) = 0;
}

void update_camera_position(Eigen::Matrix4f transformation){
    Eigen::Vector4f pos_camera, pos_world;
    pos_camera << 0,0,0,1;
    pos_world = R*transformation*pos_camera;
    camera_x = pos_world(0);
    camera_y = pos_world(1);
    camera_z = pos_world(2);

    Eigen::Matrix3f rotation_matrix = transformation.block(0,0,3,3);
    Eigen::Vector3f unit_vector = {0,0,1};
    Eigen::Vector3f help_vector = rotation_matrix*unit_vector;
    camera_phi = atan2(help_vector(0),help_vector(2));

    camera_index_z = (int) (camera_z * 10);
    if(camera_index_z < 0) camera_index_z = 0;    // camera went back from start
    camera_index_x = (int) (camera_x * 10 + 100);
}

void color_inflated_grid_with_path(pair<int,int> start, pair<int,int> cur){
    while(cur != start){
        Inflated_grid(cur.first,cur.second) = 5;
        int new_i = pathway[cur.first][cur.second].first;
        int new_j = pathway[cur.first][cur.second].second;
        cur.first = new_i;
        cur.second = new_j;
        if(new_i == -1) break;
        cout << new_i << " " << new_j << endl;
    }
}

void send_references(int64_t timestamp, float velocity, float yaw_rate){
    references_realsense_t msg_data;
    msg_data.timestamp = timestamp;
    msg_data.velocity = -velocity;
    msg_data.yaw_rate = yaw_rate;
    lcm_tag.publish("references_realsense", &msg_data);
    cout << endl << "velocity " << velocity << " yaw_rate " << yaw_rate << endl;
}

int main(int argc, char **argv) try
{   
    /*for(int i=0;i<40;i++){
        planned_path[i] = make_pair(i,100);
    }
    for(int i=40;i<73;i++){
        planned_path[i] = make_pair(40,100-i+40);
    }
    len_of_planned_path = 73;
    
    //circle from here
    for(int i=0;i<100;i++){
        float rad = 3.1415926535/2./100.*i;
        float hal_x = -2+2*cos(rad);
        float hal_z = 0+2*sin(rad);
        int index_z = (int) (hal_z * 10);
        int index_x = (int) (hal_x * 10 + 100);
        planned_path[i] = make_pair(index_z,index_x);
        cout<<index_z<<" "<<index_x<<endl;
    }
    planned_path[100] = make_pair(79,20);
    planned_path[101] = make_pair(78,20);
    planned_path[102] = make_pair(77,20);

    len_of_planned_path = 103;*/

    using std::chrono::high_resolution_clock;
    using std::chrono::duration_cast;
    using std::chrono::milliseconds;

    if(!lcm_tag.good())
        return 1;
    
    send_references(0,0,0);


    cout << "init of slam underway";


    ORB_SLAM2::System SLAM(argv[1],argv[2],ORB_SLAM2::System::RGBD,false);
    cout << "init finished";


    rs2::colorizer color_map;
    // Declare RealSense pipeline, encapsulating the actual device and sensors
    rs2::pipeline pipe;
    // Start streaming with default recommended configuration
    rs2::config cfg;
    cfg.enable_stream(RS2_STREAM_DEPTH, 0, 640, 480, RS2_FORMAT_Z16, 30);
    cfg.enable_stream(RS2_STREAM_COLOR, 0, 640, 480, RS2_FORMAT_RGB8, 30);

    rs2::pipeline_profile profile = pipe.start(cfg);
    using namespace cv;
    const auto window_name = "Display Image";
    //namedWindow(window_name, WINDOW_AUTOSIZE);

    pcl_ptr first_cloud(new pcl::PointCloud<pcl::PointXYZ> ());
    

    for (int i=0;i<600;i++)
    {
        driving_sk8o(); // driving loop, sending references to motors

        rs2::frameset data = pipe.wait_for_frames(); // Wait for next set of frames from the camera
        cout << "Got data from Camera";


        rs2::frame depth = data.get_depth_frame();
        rs2::frame rgb = data.get_color_frame();

        // Query frame size (width and height)
        const int w = depth.as<rs2::depth_frame>().get_width();
        const int h = depth.as<rs2::depth_frame>().get_height();

        const int w2 = rgb.as<rs2::video_frame>().get_width();
        const int h2 = rgb.as<rs2::video_frame>().get_height();

        // Create OpenCV matrix of size (w,h) from the colorized depth data
        cv::Mat image_depth(Size(w, h), CV_8UC3, (void*)depth.get_data(), Mat::AUTO_STEP);
        cv::Mat image_color(Size(w2, h2), CV_8UC3, (void*)rgb.get_data(), Mat::AUTO_STEP);
        double t1 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
        t1 = t1/1000;

        // double ttrack = std::chrono::duration<double>(t1).count();
        // Update the window with new data
        // imshow(window_name, image);
        cout << "we got here "<<endl;
        cv::Mat pose = SLAM.TrackRGBD(image_color,image_depth,t1);
        
        // if ORB SLAM loses track it returns empty matrix, we skip further eval 
        if(pose.empty()){
            cout << "empty matrix, likely lost track, just wait !"<<endl<<endl;
            if(i%45 == 0) delay_algorithm = true;
            continue;
        }

        Eigen::Matrix4f transform;
        cv2eigen(pose, transform);
        cout << "we made it here "<<endl;

        //cout << endl << transform << "incorrect" <<endl;
        //take inverse of homogenous function
        Eigen::Matrix3f rot = transform.block(0,0,3,3);
        Eigen::MatrixXf trans = transform.block(0,3,3,1);
        trans = 1.6/2.0/3.2*3*trans;
        //cout << endl << "rotation: "<< rot << "   translation: " << trans<<endl;
        rot = rot.inverse().eval();
        trans = -rot*trans;
        transform.block(0,3,3,1) = trans;
        transform.block(0,0,3,3) = rot;
        update_camera_position(transform);

        //cout << endl << transform << "correct" << endl;
        cout << camera_x << " " << camera_y << " " << camera_z << " " << camera_phi << "camera position" << endl;
        cout << camera_index_z << " " << camera_index_x << endl;

        
        update_pitch_roll_speed(rot);
        if(abs(pitch_speed) > 0.1 || abs(roll_speed) > 0.2){
            cout << "speed too much, delaying slam "<<endl<<endl;
            if(i%45 == 0) delay_algorithm = true;
            continue;
        }
        //cout << ", made the map!!";


        if(i==0){
            //first image only for finding rotation to floor alignment
            auto t1 = high_resolution_clock::now();
            auto inrist = rs2::video_stream_profile(depth.get_profile()).get_intrinsics();
            float pointA[3],pointB[3],pointC[3];
            float pointA2[3],pointB2[3],pointC2[3];

            const float help_indexA[2] = {float(320),float(460)};
            const float help_indexB[2] = {float(290),float(420)};
            const float help_indexC[2] = {float(350),float(420)};

            const float help_indexA2[2] = {float(325),float(465)};
            const float help_indexB2[2] = {float(295),float(425)};
            const float help_indexC2[2] = {float(355),float(425)};

            rs2_deproject_pixel_to_point(pointA,&inrist,help_indexA,depth.as<rs2::depth_frame>().get_distance(320, 460));
            rs2_deproject_pixel_to_point(pointB,&inrist,help_indexB,depth.as<rs2::depth_frame>().get_distance(290, 420));
            rs2_deproject_pixel_to_point(pointC,&inrist,help_indexC,depth.as<rs2::depth_frame>().get_distance(350, 420));

            rs2_deproject_pixel_to_point(pointA2,&inrist,help_indexA2,depth.as<rs2::depth_frame>().get_distance(325, 465));
            rs2_deproject_pixel_to_point(pointB2,&inrist,help_indexB2,depth.as<rs2::depth_frame>().get_distance(295, 425));
            rs2_deproject_pixel_to_point(pointC2,&inrist,help_indexC2,depth.as<rs2::depth_frame>().get_distance(355, 425));
            
            pointA[0] = (pointA[0]+pointA2[0])/2;
            pointA[1] = (pointA[1]+pointA2[1])/2;
            pointA[2] = (pointA[2]+pointA2[2])/2;
            pointB[0] = (pointB[0]+pointB2[0])/2;
            pointB[1] = (pointB[1]+pointB2[1])/2;
            pointB[2] = (pointB[2]+pointB2[2])/2;
            pointC[0] = (pointC[0]+pointC2[0])/2;
            pointC[1] = (pointC[1]+pointC2[1])/2;
            pointC[2] = (pointC[2]+pointC2[2])/2;

            cout << pointA[0] << " " << pointA[1] << " " << pointA[2] << "end of point A\n";
            cout << pointB[0] << " " << pointB[1] << " " << pointB[2] << "end of point B\n";
            cout << pointC[0] << " " << pointC[1] << " " << pointC[2] << "end of point C\n";

            make_rotation(pointA,pointB,pointC);

            auto t2 = high_resolution_clock::now();
            auto ms_int = duration_cast<milliseconds>(t2 - t1);

            cout << ms_int.count() << "ms\n" << endl << endl;
        }


        if(i%45 == 0 || delay_algorithm){
            delay_algorithm = false;
            
            rs2::pointcloud pc;
            rs2::points points;
            auto t1 = high_resolution_clock::now();
            //pc.map_to(rgb);
            points = pc.calculate(depth);
            

            auto pcl_points = points_to_pcl(points);
            // Executing the transformation
            pcl_ptr new_cloud(new pcl::PointCloud<pcl::PointXYZ> ());
            //cout << new_cloud->points.size() << "this is size of cloud \n";
            pcl::transformPointCloud (*pcl_points, *new_cloud, R*transform);

            add_points_to_grid(new_cloud);


            update_true_grid();
            inflate_grid(2);
            

            pair<int,int> cur = make_pair(FINISH_Z, FINISH_X);
            pair<int,int> start = make_pair(camera_index_z,camera_index_x);
            
            // first try search only over free cells
            int ret = search_algo(start,cur, false);
            //if(ret != 1) search_algo(start,cur, false);

            auto t2 = high_resolution_clock::now();
            auto ms_int = duration_cast<milliseconds>(t2 - t1);

            cout << "Search algo takes " << ms_int.count() << "ms\n" << endl << endl;

            std::ofstream file2("matrix_before.txt");
            if (file2.is_open()){
                file2 <<  True_grid << '\n';
            }

            Inflated_grid(start.first, start.second) = 5;
            //color_inflated_grid_with_path(start,cur);
            std::ofstream file("matrix.txt");
            if (file.is_open()){
                file <<  Inflated_grid << '\n';
            }
        }

        /*if(i==200){
            rs2::pointcloud pc;
            rs2::points points;

            //pc.map_to(rgb);
            auto t1 = high_resolution_clock::now();
            points = pc.calculate(depth);
            cout << "distance"<<depth.as<rs2::depth_frame>().get_distance(320,240)<<endl;
            auto pcl_points = points_to_pcl(points);
            // Executing the transformation
            pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud (new pcl::PointCloud<pcl::PointXYZ> ());
            pcl::transformPointCloud (*pcl_points,*transformed_cloud, R*transform);

            add_points_to_grid(transformed_cloud);
            cout << grid << endl;




            pcl::visualization::PCLVisualizer viewer ("Matrix transformation example");
            // Define R,G,B colors for the point cloud
            pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> source_cloud_color_handler (first_cloud, 255, 255, 255);
            // We add the point cloud to the viewer and pass the color handler
            viewer.addPointCloud (first_cloud, source_cloud_color_handler, "original_cloud");


            pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> transformed_cloud_color_handler (transformed_cloud, 230, 20, 20); // Red
            viewer.addPointCloud (transformed_cloud, transformed_cloud_color_handler, "transformed_cloud");
            viewer.addCoordinateSystem (1.0, "cloud", 0);
            viewer.setBackgroundColor(0.05, 0.05, 0.05, 0); // Setting background to a dark grey

            viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "original_cloud");

            viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "transformed_cloud");
            //viewer.setPosition(800, 400); // Setting visualiser window position
            while (!viewer.wasStopped ()) { // Display the visualiser until 'q' key is pressed
                viewer.spinOnce ();
            }



            //pcl::PointCloud< pcl::PointXYZ >::Ptr fun = convertDepthToPointXYZ (&points)
            
            //const vertex* fun = points.get_vertices();
            auto t2 = high_resolution_clock::now();
            auto ms_int = duration_cast<milliseconds>(t2 - t1);

            cout << ms_int.count() << "ms\n" << endl << endl;
            //points.export_to_ply("poincloud_from_rs.ply", rgb);

        }*/

    }
    send_references(0,0,0);


    pipe.stop();
    // Stop all threads
    SLAM.Shutdown();

    // Save camera trajectory
    SLAM.SaveTrajectoryTUM("CameraTrajectory.txt");
    SLAM.SaveKeyFrameTrajectoryTUM("KeyFrameTrajectory.txt");  
    //SLAM.SaveMapPoints("MapPointsSave.txt");



    return 0;
}
catch (const rs2::error & e)
{
    std::cerr << "RealSense error calling " << e.get_failed_function() << "(" << e.get_failed_args() << "):\n    " << e.what() << std::endl;
    return EXIT_FAILURE;
}
catch (const std::exception& e)
{
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
}


    /*
    ofstream myfile;
    myfile.open("PointCloud.pcl");
    std::vector<ORB_SLAM2::MapPoint*> allMapPoints = SLAM.GetMap()->GetAllMapPoints();
    std::cout << "# size=" << allMapPoints.size() << std::endl;
	std::cout << "# x,y,z" << std::endl;
	for (auto p : allMapPoints) {
		Eigen::Matrix<double, 3, 1> v = ORB_SLAM2::Converter::toVector3d(p->GetWorldPos());
		myfile << v.x() << " " << v.y() << " " << v.z() << std::endl;
	}
    myfile.close();*/


        /*if(i==30){
            rs2::pointcloud pc;
            rs2::points points;
            auto t1 = high_resolution_clock::now();
            //pc.map_to(rgb);
            points = pc.calculate(depth);
            auto pcl_points = points_to_pcl(points);
            // Executing the transformation
            pcl::transformPointCloud (*pcl_points, *first_cloud, R*transform);
            add_points_to_grid(first_cloud);

            update_true_grid();
            inflate_grid(2);


            pair<int,int> cur = make_pair(100,100);
            pair<int,int> start = make_pair(0,100);
            search_algo(start,cur);
            auto t2 = high_resolution_clock::now();
            auto ms_int = duration_cast<milliseconds>(t2 - t1);

            cout << "Search algo takes " << ms_int.count() << "ms\n" << endl << endl;
            color_inflated_grid_with_path(start,cur);

            cout << Inflated_grid << endl;
            std::ofstream file("matrix.txt");
            if (file.is_open()){
                file <<  Inflated_grid << '\n';
            }
            std::ofstream file2("matrix_before.txt");
            if (file2.is_open()){
                file2 <<  True_grid << '\n';
            }

        }*/
