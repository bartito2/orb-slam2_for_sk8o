import numpy as np
import matplotlib.pyplot as plt


def isDigit(x):
    try:
        float(x)
        return True
    except ValueError:
        return False



plt.xlabel('x [meters]')
plt.ylabel('z [meters]')


f = open('CameraTrajectory.txt','r')
Lines = f.readlines()
count = 0
old = [0,0]
for line in Lines:
	count += 1
	if count % 15 != 0:
		continue
	nums = [float(s) for s in line.split() if isDigit(s)]
	if len(nums) > 0:
		plt.plot([old[0],nums[1]],[old[1],nums[3]],'.r-' )
		old = [nums[1],nums[3]]
		print(nums[1], nums[3])

plt.grid()
plt.show()
