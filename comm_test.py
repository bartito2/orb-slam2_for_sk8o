import lcm
import references_t

def my_handler(channel, data):
    msg = references_t.references_t.decode(data)
    print("Received message on channel \"%s\"" % channel)

lc = lcm.LCM("udpm://239.255.76.67:7667?ttl=255")
subscription = lc.subscribe("references_command", my_handler)
try:
    while True:
        lc.handle()
except KeyboardInterrupt:
    pass


